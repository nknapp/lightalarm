import React from "react";
import { LoadAndRender } from "../RenderAsync";
import { fetchAlarms } from "../../apiclient/alarms/fetchAlarms";
import { Button, DatePicker, Table } from "antd";
import Icon, { BellOutlined, BellTwoTone } from "@ant-design/icons";
import { Alarm, nextRepeatValue } from "../../apiclient/alarms/model";
import { updateAlarm } from "../../apiclient/alarms/updateAlarm";
import moment from "moment";
import locale from "antd/es/date-picker/locale/de_DE";

const BellMuted: React.FC = () => {
  return (
    <span style={{ display: "inline-block", position: "relative" }}>
      <BellOutlined style={{ position: "absolute" }} />
      <svg width={15} height={15}>
        <line
          x1={0}
          y1={15}
          x2={15}
          y2={0}
          stroke={"black"}
          fill={"black"}
          strokeWidth={2}
        />
      </svg>
    </span>
  );
};

type AlarmsTableProps = {
  alarms: Alarm[];
  updateAlarm: (alarm: Alarm) => void;
};

const AlarmsTable: React.FC<AlarmsTableProps> = ({ alarms, updateAlarm }) => {
  return (
    <Table
      pagination={false}
      dataSource={alarms}
      rowKey={(row) => row.id}
      columns={[
        {
          title: "Time",
          dataIndex: "time",
          key: "time",
          render(value, alarm) {
            return (
              <DatePicker
                showTime
                locale={locale}
                disabledDate={(currentDate) => {
                  console.log(currentDate);
                  return currentDate.isBefore(moment());
                }}
                format="ddd D.M.YYYY H:mm"
                value={moment(value)}
                onChange={(newValue) => {
                  if (newValue != null) {
                    updateAlarm({ ...alarm, time: newValue.toISOString() });
                  }
                }}
              />
            );
          },
        },
        {
          title: "Repeat",
          dataIndex: "repeat",
          key: "repeat",
          render(value, alarm) {
            return (
              <Button
                type={alarm.active ? "primary" : "default"}
                onClick={() =>
                  updateAlarm({ ...alarm, repeat: nextRepeatValue(value) })
                }
              >
                {value}
              </Button>
            );
          },
        },
        {
          title: "active",
          dataIndex: "active",
          key: "active",
          render(value, alarm) {
            return value ? (
              <BellTwoTone
                onClick={() => updateAlarm({ ...alarm, active: false })}
              />
            ) : (
              <Icon
                component={BellMuted}
                style={{ opacity: 0.5 }}
                onClick={() => updateAlarm({ ...alarm, active: true })}
              />
            );
          },
        },
      ]}
    />
  );
};

export const Alarms: React.FC = () => {
  return (
    <LoadAndRender
      loader={fetchAlarms}
      render={(value, refresh) => {
        async function updateAndRefresh(alarm: Alarm): Promise<void> {
          await updateAlarm(alarm);
          refresh();
        }

        return (
          <div>
            <AlarmsTable alarms={value.alarms} updateAlarm={updateAndRefresh} />
          </div>
        );
      }}
    />
  );
};
