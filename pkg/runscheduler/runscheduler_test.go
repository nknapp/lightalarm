package runscheduler

import (
	"github.com/stretchr/testify/assert"
	"knappi.org/projects/lightalarm/pkg/scheduler"
	"os"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func setup() {
}

func TestNextWakeUpTime_sets_100_per_cent_at_the_wakeup_time(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	scheduler.UpdateAlarm(0, scheduler.WakeUpTime{Id: "0", NextTime: baseTime, Active: true, Repeat: scheduler.Never})

	assert.Equal(t, 1.0, SetCorrectBrightness(baseTime))
}

func TestNextWakeUpTime_sets_0_per_cent_30_minutes_before_wakeup_time(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	scheduler.UpdateAlarm(0, scheduler.WakeUpTime{Id: "0", NextTime: baseTime, Active: true, Repeat: scheduler.Never})

	assert.Equal(t, 0.0, SetCorrectBrightness(baseTime.Add(-30*time.Minute)))
}

func TestNextWakeUpTime_sets_50_per_cent_15_minutes_before_wakeup_time(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	scheduler.UpdateAlarm(0, scheduler.WakeUpTime{Id: "0", NextTime: baseTime, Active: true, Repeat: scheduler.Never})

	assert.Equal(t, 0.5, SetCorrectBrightness(baseTime.Add(-15*time.Minute)))
}
