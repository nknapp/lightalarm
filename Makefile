PATH := ${GOPATH}/bin:$(PATH)

TARGET_USER = pi
TARGET_HOST = pi1
TARGET_DIR = code
ARM_VERSION = 6


test:
	go test ./...

dev:
	go run app/main.go

build-raspi:
	GOOS=linux GOARCH=arm GOARM=${ARM_VERSION} go build -o target/lightalarm  app/main.go

build-frontend:
	rm -rf app/frontend
	( cd frontend && yarn build )
	cp -rf frontend/build app/frontend

build-intel:
	go build -o target/lightalarm-intel app/main.go



raspi: build-raspi
	ssh pi@lightalarm sudo systemctl stop lightalarm || true
	scp target/lightalarm pi@lightalarm:/home/pi/lightalarm
	ssh pi@lightalarm sudo systemctl start lightalarm

raspi-all: build-frontend build-raspi