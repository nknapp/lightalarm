import * as t from "io-ts";
import { lightAlarmAxios } from "../client";
import { validate } from "../../utils/io-ts-utils";
import { alarmType } from "./model";

const AlarmsState = t.type({
  alarms: t.array(alarmType),
});

export async function fetchAlarms(): Promise<t.TypeOf<typeof AlarmsState>> {
  const response = await lightAlarmAxios.get("/api/alarms");
  validate(response.data, AlarmsState);
  return response.data;
}
