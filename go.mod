module knappi.org/projects/lightalarm

go 1.16

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/robfig/cron/v3 v3.0.0 // indirect
	github.com/stianeikeland/go-rpio/v4 v4.4.0
	github.com/stretchr/testify v1.4.0
)
