package scheduler

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"time"
)

type RepeatType string

const (
	Weekly RepeatType = "Weekly"
	Daily             = "Daily"
	Never             = "Never"
)

const MaxAlarms = 5
const SunriseDuration = time.Minute * 30

type WakeUpTime struct {
	Id       string     `json:"id"`
	NextTime time.Time  `json:"time"`
	Active   bool       `json:"active"`
	Repeat   RepeatType `json:"repeat"`
}

type AlarmsType struct {
	Alarms [MaxAlarms]WakeUpTime `json:"alarms"`
}

var state = AlarmsType{
	Alarms: [MaxAlarms]WakeUpTime{
		{"0", time.Date(2020, 01, 01, 8, 0, 0, 0, time.Local), false, Never},
		{"1", time.Date(2020, 01, 01, 8, 0, 0, 0, time.Local), false, Never},
		{"2", time.Date(2020, 01, 01, 8, 0, 0, 0, time.Local), false, Never},
		{"3", time.Date(2020, 01, 01, 8, 0, 0, 0, time.Local), false, Never},
		{"4", time.Date(2020, 01, 01, 8, 0, 0, 0, time.Local), false, Never},
	},
}

func Reset() {
	for index, alarm := range state.Alarms {
		state.Alarms[index] = WakeUpTime{alarm.Id, time.Date(2020, 01, 01, 8, 0, 0, 0, time.Local), false, Never}
	}
}

func Load(dataFile string) (err error) {
	fileContents, err := ioutil.ReadFile(dataFile)
	if err != nil {
		log.Print("Error loading data from from " + dataFile)
		return err
	}
	err = json.Unmarshal(fileContents, &state)
	if err != nil {
		log.Print("Error unmarshalling data from " + dataFile)
		return err
	}
	return err
}

func UpdateAlarm(slot int8, alarm WakeUpTime) {
	state.Alarms[slot] = alarm
}

func Save(dataFile string) error {
	jsonString, err := json.MarshalIndent(state, "", "  ")
	if err != nil {
		log.Print("Error marshalling alarms")
		return err
	}

	err = ioutil.WriteFile(dataFile, jsonString, 0644)
	if err != nil {
		log.Print("Error writing to " + dataFile)
		return err
	}

	return nil
}

func GetAlarms() AlarmsType {
	return state
}

func UpdateNextInvocations(currentTime time.Time) {
	for index, alarm := range state.Alarms {
		switch repeat := alarm.Repeat; repeat {
		case Daily:
			state.Alarms[index].NextTime = increaseDailyUntilAfter(state.Alarms[index].NextTime, currentTime)
		case Weekly:
			state.Alarms[index].NextTime = increaseWeeklyUntilAfter(state.Alarms[index].NextTime, currentTime)
		case Never:
			// Do not update alarm
		}
	}
}

func increaseDailyUntilAfter(nextTime time.Time, currentTime time.Time) time.Time {
	currentTimeWithThreshold := currentTime.Add(-SunriseDuration)
	for nextTime.Before(currentTimeWithThreshold) {
		nextTime = nextTime.AddDate(0, 0, 1)
	}
	return nextTime
}

func increaseWeeklyUntilAfter(nextTime time.Time, currentTime time.Time) time.Time {
	currentTimeWithThreshold := currentTime.Add(-SunriseDuration)
	for nextTime.Before(currentTimeWithThreshold) {
		nextTime = nextTime.AddDate(0, 0, 7)
	}
	return nextTime
}

func NextWakeUpTime(currentTime time.Time) int {
	startOfSunrise := currentTime.Add(-SunriseDuration)
	var result = -1
	for index, alarm := range state.Alarms {
		if isValidWakeUpTime(alarm, startOfSunrise) {
			if result < 0 || alarm.NextTime.Before(state.Alarms[result].NextTime) {
				result = index
			}
		}
	}
	return result
}

func isValidWakeUpTime(wakeUpTime WakeUpTime, startOfSunrise time.Time) bool {
	return wakeUpTime.Active && startOfSunrise.Before(wakeUpTime.NextTime)
}
