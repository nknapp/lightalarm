package gpio

import (
	"github.com/stianeikeland/go-rpio/v4"
	"log"
)

var (
	pin18 rpio.Pin
)

func InitGpio() error {
	err := rpio.Open()
	if err != nil {
		return err
	}

	pin18 = rpio.Pin(18)
	pin18.Pwm() // Output mode
	pin18.Freq(4000000)
	return nil
}

func SetPin18Brightness(level float64) {
	dutyLen := uint32(level * 10000.0)
	log.Printf("Set dutyLen to %f\n", dutyLen)
	pin18.DutyCycle(dutyLen, 10000)
}

func TurnPin18Off() error {
	pin18.Low()
	return rpio.Close()
}
