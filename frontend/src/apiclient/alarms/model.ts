import * as t from "io-ts";

export const repeatValues = ["Never", "Daily", "Weekly"] as const;
type RepeatValue = typeof repeatValues[number];

export function nextRepeatValue(currentValue: RepeatValue): RepeatValue {
  const index = repeatValues.indexOf(currentValue);
  return repeatValues[(index + 1) % repeatValues.length];
}

export const alarmType = t.type({
  id: t.string,
  time: t.string,
  active: t.boolean,
  repeat: t.union([
    t.literal<RepeatValue>("Never"),
    t.literal<RepeatValue>("Daily"),
    t.literal<RepeatValue>("Weekly"),
  ]),
});

export type Alarm = t.TypeOf<typeof alarmType>;
