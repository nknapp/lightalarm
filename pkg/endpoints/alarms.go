package endpoints

import (
	"github.com/gin-gonic/gin"
	"knappi.org/projects/lightalarm/pkg/scheduler"
	"net/http"
	"strconv"
	"time"
)

func AddAlarmEndpoints(router *gin.RouterGroup, dataFile string) {
	scheduler.Load(dataFile)
	scheduler.UpdateNextInvocations(time.Now())

	router.GET("/alarms", func(c *gin.Context) {
		alarms := scheduler.GetAlarms()
		c.JSON(200, gin.H{
			"alarms": alarms.Alarms,
		})
	})

	router.PUT("/alarms/:id", func(c *gin.Context) {
		slot, err := strconv.ParseInt(c.Param("id"), 10, 7)
		alarm := scheduler.WakeUpTime{}
		err = c.ShouldBind(&alarm)
		if err != nil {
			c.JSON(http.StatusBadRequest, `The body should be an alarm`)
			return
		}
		slotInt8 := int8(slot)
		scheduler.UpdateAlarm(slotInt8, alarm)
		scheduler.UpdateNextInvocations(time.Now())
		scheduler.Save(dataFile)
		c.JSON(200, scheduler.GetAlarms().Alarms[slotInt8])
	})
}
