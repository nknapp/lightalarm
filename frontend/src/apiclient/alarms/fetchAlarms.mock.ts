export const validAlarms = {
    "alarms": [
    {
        "id": "0",
        "active": false,
        "repeat": "Never",
        "time": "2020-01-01T08:00:00+01:00"
    },
    {
        "id": "1",        "active": false,
        "repeat": "Weekly",
        "time": "2020-01-01T08:00:00+01:00"
    },
    {
        "id": "2",
        "active": false,
        "repeat": "Daily",
        "time": "2020-01-01T08:00:00+01:00"
    },
    {
        "id": "3",
        "active": false,
        "repeat": "Never",
        "time": "2020-01-01T08:00:00+01:00"
    },
    {
        "id": "4",
        "active": false,
        "repeat": "Never",
        "time": "2020-01-01T08:00:00+01:00"
    }
]
}