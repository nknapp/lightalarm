package main

import (
	"embed"
	"github.com/gin-gonic/gin"
	"knappi.org/projects/lightalarm/pkg/endpoints"
	"knappi.org/projects/lightalarm/pkg/runscheduler"
	"log"
	"net/http"
	"strings"
)

//go:embed frontend/*
var frontendStaticFs embed.FS

func main() {
	runscheduler.Run()

	router := gin.Default()
	router.Use(func(c *gin.Context) {
		requestURI := c.Request.RequestURI
		if strings.HasPrefix(requestURI, "/api") {
			c.Next()
			return
		}
		filepath := "frontend" + requestURI
		c.FileFromFS(filepath, http.FS(frontendStaticFs))
	})
	api := router.Group("/api")
	endpoints.AddAlarmEndpoints(api, "alarms.json")
	endpoints.AddTestingEndpoints(api)
	runErr := router.Run()

	if runErr != nil {
		log.Fatal(runErr)
	}

}
