import * as t from "io-ts";
import axios from "axios";
import {validate} from "../../utils/io-ts-utils";

const MockTimeResponseType = t.type({
    brightness: t.number,
});

export async function testWithMockTime(time: string): Promise<number> {
    const result = await axios.put(`/api/mockTime`, {Time: time});
    validate(result.data, MockTimeResponseType)
    return result.data.brightness
}