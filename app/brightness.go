package main

import (
	"github.com/stianeikeland/go-rpio/v4"
)

var (
	pin18 rpio.Pin
	pin25 rpio.Pin
)

func InitGpio() error {
	err := rpio.Open()
	if err != nil {
		return err
	}

	pin18 = rpio.Pin(18)
	pin18.Pwm() // Output mode
	pin18.Freq(4000000)
	return nil
}

func SetBrightness(level uint32) {
	pin18.DutyCycle(level, 10000)
}

func Off() error {
	pin18.Low()
	pin25.Low()
	return rpio.Close()
}
