import locale from "antd/es/date-picker/locale/de_DE";
import moment from "moment";
import { DatePicker } from "antd";
import React, { useState } from "react";
import { testWithMockTime } from "../apiclient/testing/mockTime";

export const MockTime: React.FC = () => {
  const [mockBrightness, setMockBrightness] = useState(0);
  const [mockTime, setMockTime] = useState(moment());
  return (
    <div style={{ display: "flex", alignItems: "center" }}>
      <DatePicker
        showTime
        showNow
        locale={locale}
        value={mockTime}
        onChange={(newValue) => {
          setMockTime(mockTime);
          if (newValue != null) {
            testWithMockTime(newValue.toISOString()).then(setMockBrightness);
          }
        }}
      />
      <span style={{ marginLeft: "1rem" }}>
        Brightness: {(mockBrightness * 100).toFixed(2)}%
      </span>
    </div>
  );
};
