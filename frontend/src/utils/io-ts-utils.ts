import {Type} from "io-ts";
import {PathReporter} from "io-ts/PathReporter";

export function validate<T>(value: T, type: Type<T>): asserts value is T {
    if (type.is(value)) {
        return
    }
    const decodedValue = type.decode(value);
    throw new Error("ValidationError: " + PathReporter.report(decodedValue));
}