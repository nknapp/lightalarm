import {RequestHandler} from "msw";
import {setupServer} from "msw/node";

type MockApiResult = {
  overrideMockApi(...requestHandlers: RequestHandler[]): void;
};

export function mockApiJest(
  ...requestHandlers: RequestHandler[]
): MockApiResult {
  const server = setupServer(...requestHandlers);
  beforeAll(async () => {
    await server.listen({
        onUnhandledRequest: "error"
    });
  });

  beforeEach(() => {
      server.resetHandlers();
  })

  afterAll(async () => {
    await server.close();
  });

  return {
    overrideMockApi(...requestHandlers) {
      server.use(...requestHandlers);
    },
  };
}
