package runscheduler

import (
	"fmt"
	"knappi.org/projects/lightalarm/pkg/gpio"
	"knappi.org/projects/lightalarm/pkg/scheduler"
	"time"
)

const tickDelaySeconds = 5

var done = make(chan bool)

func Run() {
	gpio.InitGpio()
	ticker := time.NewTicker(tickDelaySeconds * time.Second)
	go func() {
		for {
			select {
			case <-done:
				return
			case <-ticker.C:
				SetCorrectBrightness(time.Now())
			}
		}
	}()
}

func SetCorrectBrightness(now time.Time) float64 {
	scheduler.UpdateNextInvocations(now)
	nextAlarmIndex := scheduler.NextWakeUpTime(now)
	if nextAlarmIndex < 0 {
		gpio.SetPin18Brightness(0)
		return 0
	}
	nextWakeUpTime := scheduler.GetAlarms().Alarms[nextAlarmIndex].NextTime
	sunriseRunningFor := now.Sub(nextWakeUpTime) + scheduler.SunriseDuration
	if sunriseRunningFor < 0 || sunriseRunningFor > scheduler.SunriseDuration {
		gpio.SetPin18Brightness(0)
		return 0
	}
	brightness := sunriseRunningFor.Minutes() / scheduler.SunriseDuration.Minutes()
	gpio.SetPin18Brightness(brightness)
	return brightness
}

func Stop() {
	done <- true
	fmt.Println("Stopped")
}
