import { mockApiJest } from "../../utils/test/mock-api-jest";
import { rest } from "msw";
import { fetchAlarms } from "./fetchAlarms";
import { validAlarms } from "./fetchAlarms.mock";

const { overrideMockApi } = mockApiJest(
  rest.get("/api/alarms", (req, res, context) => {
    return res(context.json(validAlarms));
  })
);

test("returns valid states", async () => {
  expect(await fetchAlarms()).toEqual(validAlarms);
});

test("throws on invalid states", async () => {
  overrideMockApi(
    rest.get("/api/alarms", (req, res, context) => {
      return res(context.json({ invalidField: "test" }));
    })
  );
  await expect(fetchAlarms()).rejects.toThrow(/ValidationError/);
});
