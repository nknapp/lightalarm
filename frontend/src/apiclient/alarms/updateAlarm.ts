import {Alarm} from "./model";
import axios from "axios";

export async function updateAlarm(alarm: Alarm): Promise<void> {
  await axios.put(`/api/alarms/${alarm.id}`, alarm);
}
