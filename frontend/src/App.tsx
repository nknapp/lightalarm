import React from "react";
import logo from "./logo.svg";

import "antd/dist/antd.css";

import "./App.css";

import { Alarms } from "./components/Alarms/Alarms";
import {Collapse, Layout} from "antd";
import {MockTime} from "./components/MockTime";
const { Header, Footer, Content } = Layout;

function App() {
  return (
    <Layout
      style={{ maxWidth: "800px", marginLeft: "auto", marginRight: "auto" }}
    >
      <Header>Light alarm</Header>
      <Content>
        <Alarms />

      </Content>
      <Collapse defaultActiveKey={[]}>
        <Collapse.Panel header="Test with mocked time" key="0">
          <MockTime />
        </Collapse.Panel>
      </Collapse>,
      <Footer>Copyright (c) Nils Knappmeier {new Date().getFullYear()}</Footer>
    </Layout>
  );
}

export default App;
