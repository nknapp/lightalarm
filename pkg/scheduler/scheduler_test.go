package scheduler

import (
	"github.com/stretchr/testify/assert"
	"log"
	"os"
	"testing"
	"time"
)

const tmpDir = "test-tmp"

func TestMain(m *testing.M) {
	setup()
	code := m.Run()
	os.Exit(code)
}

func setup() {
	var err = os.RemoveAll(tmpDir)
	if err != nil {
		log.Fatal("error while creating tmp dir '" + tmpDir + "'")
	}
	err = os.MkdirAll(tmpDir, 0755)
	if err != nil {
		log.Fatal("error while creating tmp dir '" + tmpDir + "'")
	}
	Reset()
}

func TestSaveUpdateRevert(t *testing.T) {
	var err = Save(tmpDir + "/alarms.json")

	UpdateAlarm(0, WakeUpTime{"0", time.Date(2020, 02, 01, 8, 0, 0, 0, time.Local), false, Never})
	if err != nil {
		t.Fail()
	}

	assert.Equal(t, time.Month(2), GetAlarms().Alarms[0].NextTime.Month(), "Month should have been updated")

	err = Load(tmpDir + "/alarms.json")
	if err != nil {
		t.Fail()
	}
	assert.Equal(t, time.Month(1), GetAlarms().Alarms[0].NextTime.Month(), "Month should have been reverted")
}

func TestUpdateAlarmsNoRepeat(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := time.Date(2020, 2, 5, 4, 0, 0, 0, time.Local)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Never})
	UpdateNextInvocations(currentTime)

	assert.Equal(t, originalTime, GetAlarms().Alarms[0].NextTime, "Time should not change")
}

func TestUpdateAlarmsDailyRepeat(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := time.Date(2020, 2, 5, 4, 0, 0, 0, time.Local)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Daily})
	UpdateNextInvocations(currentTime)

	expectedTime := time.Date(2020, 2, 5, 8, 0, 0, 0, time.Local)
	assert.Equal(t, expectedTime, GetAlarms().Alarms[0].NextTime, "Time update to the same time on the current day")
}

func TestUpdateAlarmsDailyRepeatPassedAlready(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := time.Date(2020, 2, 5, 10, 0, 0, 0, time.Local)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Daily})
	UpdateNextInvocations(currentTime)

	expectedTime := time.Date(2020, 2, 6, 8, 0, 0, 0, time.Local)
	assert.Equal(t, expectedTime, GetAlarms().Alarms[0].NextTime, "Time update to the same time on next day")
}

func TestUpdateAlarmsDailyRepeatPassedAlreadyButWithinThreshold(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := originalTime.Add(time.Minute * 30)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Daily})
	UpdateNextInvocations(currentTime)

	assert.Equal(t, originalTime, GetAlarms().Alarms[0].NextTime, "Time should not update")
}

func TestUpdateAlarmsWeeklyRepeat(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := time.Date(2020, 2, 5, 4, 0, 0, 0, time.Local)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Weekly})
	UpdateNextInvocations(currentTime)

	expectedTime := time.Date(2020, 2, 8, 8, 0, 0, 0, time.Local)
	assert.Equal(t, expectedTime, GetAlarms().Alarms[0].NextTime, "Time update to the same time on the current day")
}

func TestUpdateAlarmsWeeklyRepeatPassedAlready(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := time.Date(2020, 2, 5, 10, 0, 0, 0, time.Local)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Weekly})
	UpdateNextInvocations(currentTime)

	expectedTime := time.Date(2020, 2, 8, 8, 0, 0, 0, time.Local)
	assert.Equal(t, expectedTime, GetAlarms().Alarms[0].NextTime, "Time update to the same time on next day")
}

func TestUpdateAlarmsWeeklyRepeatPassedAlreadyButWithinThreshold(t *testing.T) {
	originalTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	currentTime := originalTime.Add(time.Minute * 30)

	UpdateAlarm(0, WakeUpTime{"0", originalTime, false, Daily})
	UpdateNextInvocations(currentTime)

	assert.Equal(t, originalTime, GetAlarms().Alarms[0].NextTime, "Time should not update")
}

func TestNextWakeUpTime_finds_the_earliest_time(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	UpdateAlarm(0, WakeUpTime{"0", baseTime.Add(time.Hour * 1), true, Never})
	UpdateAlarm(1, WakeUpTime{"1", baseTime.Add(time.Hour * 0), true, Never})
	UpdateAlarm(2, WakeUpTime{"2", baseTime.Add(time.Hour * 2), true, Never})

	assert.Equal(t, 1, NextWakeUpTime(baseTime.Add(-time.Hour)))
}

func TestNextWakeUpTime_omits_alarms_that_are_finished_already(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	UpdateAlarm(0, WakeUpTime{"0", baseTime.Add(time.Hour * 1), true, Never})
	UpdateAlarm(1, WakeUpTime{"1", baseTime.Add(time.Hour * 0), true, Never})
	UpdateAlarm(2, WakeUpTime{"2", baseTime.Add(time.Hour * 2), true, Never})

	assert.Equal(t, 0, NextWakeUpTime(baseTime.Add(time.Minute*29+time.Hour*1)))
}

func TestNextWakeUpTime_omits_inactive_alarms(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	UpdateAlarm(0, WakeUpTime{"0", baseTime.Add(time.Hour * 1), false, Never})
	UpdateAlarm(1, WakeUpTime{"1", baseTime.Add(time.Hour * 0), false, Never})
	UpdateAlarm(2, WakeUpTime{"2", baseTime.Add(time.Hour * 2), true, Never})

	assert.Equal(t, 2, NextWakeUpTime(baseTime.Add(-time.Hour)))
}

func TestNextWakeUpTime_returns_minus_one_if_there_is_no_alarm(t *testing.T) {
	baseTime := time.Date(2020, 2, 1, 8, 0, 0, 0, time.Local)
	UpdateAlarm(0, WakeUpTime{"0", baseTime.Add(time.Hour * 1), true, Never})
	UpdateAlarm(1, WakeUpTime{"1", baseTime.Add(time.Hour * 0), true, Never})
	UpdateAlarm(2, WakeUpTime{"2", baseTime.Add(time.Hour * 2), true, Never})

	assert.Equal(t, -1, NextWakeUpTime(baseTime.Add(time.Hour*5)))
}
