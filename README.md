Auto-start on Raspbian by adding file `/etc/systemd/system/lightalarm.service`:

```
[Unit]
After=local-fs.target

[Service]
ExecStart=/home/pi/lightalarm
Restart=on-failure
RestartSec=30

[Install]
WantedBy=multi-user.target
```

and running 

```bash
systemctl enable lightalarm
systemctl start lightalarm
```