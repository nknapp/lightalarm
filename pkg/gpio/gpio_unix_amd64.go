package gpio

import (
	"log"
)

func InitGpio() error {
	log.Println("Mock InitGpio")
	return nil
}

func SetPin18Brightness(level float64) {
	log.Printf("Mock SetPin18Brightness %f\n", level)
}

func TurnPin18Off() error {
	log.Print("Mock TurnPin18Off")
	return nil
}
