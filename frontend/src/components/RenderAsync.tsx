import {ReactElement, ReactNode, useCallback, useEffect, useState,} from "react";
import {LoadingOutlined} from "@ant-design/icons";

export type LoadAndRenderProps<T> = {
  loader: () => Promise<T>;
  render: (value: T, refresh: () => void) => ReactNode;
};

export function LoadAndRender<T>({
  loader,
  render,
}: LoadAndRenderProps<T>): ReactElement {
  const [value, setValue] = useState<T>();

  const refresh = useCallback(() => {
    loader().then(setValue);
  }, [loader]);

  useEffect(() => {
    refresh();
  }, [refresh]);

  if (value != null) {
    return <>{render(value, refresh)}</>;
  }

  return (
    <div>
      <LoadingOutlined />
    </div>
  );
}
