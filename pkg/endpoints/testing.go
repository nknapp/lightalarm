package endpoints

import (
	"github.com/gin-gonic/gin"
	"knappi.org/projects/lightalarm/pkg/runscheduler"
	"net/http"
	"time"
)

type MockTimeRequest struct {
	Time time.Time `json:"time"`
}

type MockTimeResult struct {
	Brightness float64 `json:"brightness"`
}

func AddTestingEndpoints(router *gin.RouterGroup) {
	router.PUT("/mockTime", func(c *gin.Context) {
		currentTime := MockTimeRequest{}
		err := c.ShouldBind(&currentTime)
		if err != nil {
			c.JSON(http.StatusBadRequest, `The body should contain the current time`)
			return
		}
		brightness := runscheduler.SetCorrectBrightness(currentTime.Time)
		c.JSON(200, MockTimeResult{brightness})
	})
}
